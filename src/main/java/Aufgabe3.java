public class Aufgabe3 {

    static {
        System.loadLibrary("Matrix_RELEASE_64bit");
    }

    public static void main(String[] args) {

        Matrix a = new Matrix(500, 6000);
        Matrix b = new Matrix(6000, 500);

        long startTime = System.nanoTime();
        Matrix r = a.multiply(b);
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("Time in java: " + estimatedTime);

        long startTime1 = System.nanoTime();
        Matrix sds = a.multiplyNative(b);
        long estimatedTime1 = System.nanoTime() - startTime1;
        System.out.println("Time in native function (RELEASE MODE 64-bit): " + estimatedTime1);

        System.out.println("Are both equal? " + r.equals(sds));

        /*
         * Time in java: 33919402207 
         * Time in native function (RELEASE MODE 64-bit): 30907404515 
         * Are both equal? true
         */
    }
}
