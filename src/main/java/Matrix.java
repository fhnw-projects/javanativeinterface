public class Matrix {

    private int width;
    private int height;

    private double[] matrix;

    native void multiplyC(double[] a, double[] b, double[] r, int m, int n, int o);

    public Matrix(int width, int height) {
        int size = width * height;
        this.matrix = new double[size];
        this.width = width;
        this.height = height;
        for (int i = 0; i < size; i++) {
            matrix[i] = Math.random();
        }
    }

    public Matrix(int width, int height, double defaultValue) {
        int size = width * height;
        this.matrix = new double[size];
        this.width = width;
        this.height = height;
        for (int i = 0; i < size; i++) {
            matrix[i] = defaultValue;
        }
    }

    public Matrix(int width, int height, double[] matrix) {
        this.matrix = matrix;
        this.width = width;
        this.height = height;
    }

    public Matrix multiply(Matrix m) {
        if (this.width != m.height) {
            throw new IllegalArgumentException(
                    "Matrix cant be multiplied. The height of the first matrix must be the same as the width of the second matrix");
        }

        double[] result = new double[this.height * m.width];

        for (int x = 0; x < this.height; x++) {
            for (int i = 0; i < m.width; i++) {
                double d = 0;
                for (int k = 0; k < this.width; k++) {
                    d += this.matrix[k + (x * this.width)] * m.matrix[(k * m.width) + i];
                }
                result[(x * m.width) + i] = d;
            }
        }

        return new Matrix(m.width, this.height, result);
    }

    public Matrix multiplyNative(Matrix m) {
        if (this.width != m.height) {
            throw new IllegalArgumentException(
                    "Matrix cant be multiplied. The height of the first matrix must be the same as the width of the second matrix");
        }

        double[] result = new double[this.height * m.width];
        multiplyC(this.matrix, m.matrix, result, this.height, m.width, this.width);

        return new Matrix(m.width, this.height, result);
    }

    public Matrix power(int k) {
        if (this.height != this.width) {
            throw new IllegalArgumentException("Matrix is not square");
        }

        for (int i = 0; i < k; i++) {
            multiply(this);
        }

        return this;
    }

    public Matrix powerNative(int k) {
        if (this.height != this.width) {
            throw new IllegalArgumentException("Matrix is not square");
        }

        for (int i = 0; i < k; i++) {
            multiplyNative(this);
        }

        return this;
    }

    public boolean equals(Matrix m) {
        if (this.height != m.height || this.width != m.width || this.matrix.length != m.matrix.length) {
            return false;
        }

        for (int i = 0; i < this.matrix.length; i++) {
            if (this.matrix[i] != m.matrix[i]) {
                return false;
            }
        }

        return true;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double[] getMatrix() {
        return matrix;
    }

    public void setMatrix(double[] matrix) {
        this.matrix = matrix;
    }

}
