
public class Aufgabe4 {

    static {
        System.loadLibrary("Matrix_RELEASE_64bit");
    }

    public static void main(String[] args) {

        Matrix a = new Matrix(250, 250);

        long startTime = System.nanoTime();
        Matrix r = a.power(91);
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println("Time in java: " + estimatedTime);

        long startTime1 = System.nanoTime();
        Matrix sds = a.powerNative(91);
        long estimatedTime1 = System.nanoTime() - startTime1;
        System.out.println("Time in native function (RELEASE MODE 64-bit): " + estimatedTime1);

        System.out.println("Are both equal? " + r.equals(sds));

        /*
         * Time in java: 1484360840 
         * Time in native function (RELEASE MODE 64-bit): 1094762408
         * Are both equal? true
         * 
         */
    }

}
