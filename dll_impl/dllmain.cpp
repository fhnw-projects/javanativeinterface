// dllmain.cpp : Definiert den Einstiegspunkt für die DLL-Anwendung.
#include "stdafx.h"
#include "Matrix.h"
#include <iostream>

using namespace std;

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
) {
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

JNIEXPORT void JNICALL Java_Matrix_multiplyC
(JNIEnv *env, jobject obj, jdoubleArray a, jdoubleArray b, jdoubleArray r, jint m , jint n, jint o) {
	
	jdouble *ab = env->GetDoubleArrayElements(a, false);
	jdouble *bb = env->GetDoubleArrayElements(b, false);
	jdouble *rb = env->GetDoubleArrayElements(r, false);

	for (int x = 0; x < m; x++) {
		for (int i = 0; i < n; i++) {
			 double d = 0;
			for (int k = 0; k < o; k++) {
				d += ab[k + (x * o)] * bb[(k * n) + i];
			}
			rb[(x * n) + i] = d;
		}
	}

	env->ReleaseDoubleArrayElements(a, ab, 0);
	env->ReleaseDoubleArrayElements(b, bb, 0);
	env->ReleaseDoubleArrayElements(r, rb, 0);

}

